#include "mstring.h"
#include <stdlib.h>
#include <string.h>

mstring* string_new(const char* s){
	if (s == NULL) goto error;
	mstring* ret = (mstring*)malloc(sizeof(mstring));
	if (ret == NULL) goto error;;
	ret->len = strlen(s);
	ret->str = (char*)malloc(sizeof(char)*(ret->len + 1));
	if (ret->str == NULL){
		free(ret);
		goto error;
	}
	strncpy(ret->str, s, ret->len);	

	return ret;

error:
	return NULL;
}

mstring* string_concatenate(mstring* s1, mstring* s2){
	mstring* ret;
	int len;
	len = s1->len + s2->len;
	ret = (mstring*)malloc(sizeof(mstring));
	if (ret == NULL) goto error;
	ret->str = (char*)malloc(sizeof(char)*(len+1));
	if (ret->str == NULL){
		free(ret);
		goto error;
	}
	ret->len = len;	

	return ret;

error:
	return NULL;
}

mstring* string_sub(mstring* string, int i1, int i2){
	char* buffer;
	int len, i;
	
	if ((i1 > i2) || (i1 > string->len) || (i2 > string->len)){
		goto error;
	}
	len = i2 - i1 + 2;
	buffer = (char*)malloc(sizeof(char)*len);
	for (i = 0; i < len-1; i++){
		buffer[i] = string->str[i];
	}
	buffer[len-1] = '\0';
	return string_new(buffer); 	

error:
	return NULL;	
}

mstring* string_tolower(const mstring* string){
	char* s;
	mstring* ret;
	
	s = malloc(sizeof(char)*(string->len+1));
	if (s == NULL) goto error;

	strcpy(s, string->str);
	for (int i = 0; s[i] != '\0'; i++){
        if (s[i] >= 65 && s[i] <= 90)
            s[i] += 32;
    }
	
	ret = string_new(s);
	if (ret == NULL) goto error;

error:
	return NULL;
}

mstring* string_reverse(const mstring* string){
	char* s;
	int i;
	mstring* ret;

	s = malloc(sizeof(char) * (string->len + 1));
	if (s == NULL) goto error;
	for (i = 0; i < string->len; i++){
		s[0] = string->str[string->len - i];
	}

	ret = string_new(s);
	if (ret == NULL){
		free(s);
		goto error;
	}

	return ret;

error:
	return NULL;
}

void string_delete(mstring* string){
	free(string->str);
	free(string);
}
