#ifndef M_LIST_H
#define M_LIST_H

#define LIST_INITIAL_INDEX 0
#define list_iterate(L) for(unsigned int __LISTITERATOR__ = LIST_INITIAL_INDEX; __LISTITERATOR__ < L->size + LIST_INITIAL_INDEX; __LISTITERATOR__++)
#define PTR void*

typedef struct list_slot{
	struct list_slot* next;
	struct list_slot* previous;
	unsigned int index;
	PTR element;
} list_slot_t;

typedef struct {
	char* name;
	unsigned int size;
	list_slot_t* start;
	list_slot_t* last;
} mlist;

mlist* list_create();
PTR list_get(mlist* List, unsigned int index);
int list_get_index(mlist* List, PTR element);
void list_add(mlist* List, PTR element);
void list_remove(mlist* List, unsigned int index);
PTR list_pop(mlist* List, unsigned int index);
void list_reverse();
void list_replace_element(mlist* List, unsigned int index1, unsigned int index2);
mlist* list_copy(mlist* List);
void list_destroy(mlist* List);
PTR* list_copy_to_array(mlist* List);
void list_sort_int(mlist* List);
void quicksort(int* array, int size);

extern mlist* all_lists;

#undef PTR
#endif /* M_LIST_H */