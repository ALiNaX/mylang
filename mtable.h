#ifndef MTABLE_H
#define MTABLE_H

struct mtable_s;
typedef struct mtable_s mtable;

mtable* table_create_presized(size_t cap);
mtable* table_create();
void*   table_get();
void table_append(mtable* tab, size_t index);
mtable* table_get(mtable* tab, size_t index);
void table_remove(mtable* tab, size_t index);
void table_remove(mtable* tab);

#endif /* MTABLE_H */
