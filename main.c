#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include "mlist.h"

#if defined(WIN32) || defined(_WIN32)
    #include <conio.h>
    #include <windows.h>
    #define _GETCH printf("\n\nPress any key to leave program..."); getch()
#else
    #define _GETCH
#endif

#include "mlist.h"
#include "mstring.h"
#include "lang.h"

#define MVERSION 0.1.0
#define PTR void*
#define MAXINPUTSIZE 1000
#define INPUT_MODE_BUFFER_SIZE 100
#define CODE_EXTENSION ".ml"

int main(int argc, char** argv) {
    char input[MAXINPUTSIZE];
    char* codebuffer;
    char c;
    FILE* codefile;
    mstate* globalstate = state_create(NULL);
    size_t codebuffer_size = INPUT_MODE_BUFFER_SIZE;

    currentstate = globalstate;
    if (argc == 1) goto no_input_mode;


    //If program opens with file...
    //Check file extension:
    char* _tempchar = (char*)malloc(sizeof(char)*4);
    strncpy(_tempchar, &(argv[1][strlen(argv[1]) - 3]), 4);
    if ((strcmp(_tempchar, CODE_EXTENSION))){
        printf("Error: Expected '.ml' file, got %s\n",_tempchar);
    }
    else {
        free(_tempchar);
        goto input_mode;
    }

    _GETCH;
    return 0;

input_mode:
    codefile = fopen(argv[1], "r");

    for (int i = 0; codebuffer[i] != EOF; i++){
        if (i > codebuffer_size){
            codebuffer = realloc(codebuffer, codebuffer_size + INPUT_MODE_BUFFER_SIZE);
            if (codebuffer == NULL) {
                printf("Error: out of memory!");
                exit(1);
            }
            codebuffer_size += INPUT_MODE_BUFFER_SIZE;
        }
        codebuffer[i] = getc(codefile);
        if (codebuffer[i] == '\n'){
            codebuffer[i] = '\0';
            if (!strcmp(codebuffer, "write(jooj)")){
                printf("%d\n", var_find(globalstate, "jooj")->value.integer);
                i = -1;
                continue;
            }
            readcmd(codebuffer);
            i = -1;
        }
    }

    printf("\n");

    fclose(codefile);
    _GETCH;
    return 0;

no_input_mode:
    printf("Welcome to MyLang!\n");
    for(;;){
        printf("> ");
        fgets(input, MAXINPUTSIZE, stdin);
        if (!strcmp(input, "write(jooj)\n")){
            printf("%d\n", var_find(globalstate, "jooj")->value.integer);
            continue;
        }
        if (!strcmp(input, "write(asd)\n")){
            printf("%d\n", var_find(globalstate, "asd")->value.boolean);
            continue;
        }
        if (!strcmp(input, "write(asdf)\n")){
            printf("%s\n", var_find(globalstate, "asdf")->value.string->str);
            continue;
        }
        readcmd(input);
    }

    _GETCH;
    return 0;
}
