#include "mtable.h"
#include <stdlib.h>
#include <stdio.h>

struct mtable_s {
	size_t len;
	size_t cap;
	void** elements;
};

//Creates a table with pre-defined size. Altough not mandatory,
//pre-setting a size improves performance since it prevents reallocs.
mtable* table_create_alloc(size_t cap){
	mtable* ret = (mtable*)malloc(sizeof(mtable));
	if (ret == NULL) goto error;
	
	ret->elements = (void**)malloc(sizeof(void*)*cap);
	
	if (ret->elements == NULL){
		free(ret);
		goto error;
	}
	ret->len = 0;
	ret->cap = cap;
	
	return ret;

error:
	fprintf(stderr, "Error: Out of memory.\n");
	return NULL;
}

//Creates a table and returns its pointer. Default cap is 4.
mtable* table_create(){
	return table_create_alloc(4);
}

//Returns the element contained at the specified table at the given index.
//Returns null in case the specified index is invalid.
void* table_get(mtable* tab, size_t index){
	if (index < len){
		return tab->elemets[index];
	}

	fprintf(stderr, "Error: index out of table bounds.\n");
	return NULL;
}

//Returns the index of the given element in the table.
//Returns -1 in case the given element is not found.
int table_find(mtable* tab, void* el){
	for (int i = 0; i < tab->len; i++){
		if (tab->elements[i] == el) return i;
	}
	return -1;
}

//Changes the element at the specified index to point to the element
//specified at 'el'.
void table_set(mtable* tab, size_t index, void* el){
	if (index < tab->len){
		tab->elements[index] = el;
	} else goto error;

error:
	fprintf(stderr, "Error: index out of list bounds.\n");
}

//Adds an element to the end of the table.
void table_append(mtable* tab, void* el){
	if (tab->len < tab->cap){
		tab->elements[tab->len] = el;
		tab->len++;
	}
	else { 
		tab->elements = (void**)realloc(tab->element, tab->cap*2);
		if (tab->elements == NULL) goto error;
		tab->cap *= 2;
		tab->elements[tab->len] = el;
		tab->len++;
	}

error:
	fprintf(stderr, "Error: Out of memory.\n");
}

//Removes the specified element from the table.
void table_remove(mtable* tab, size_t index){
	for (int i = index; i < tab->len - 1; i++){
		tab->elements[i] = tab->elements[i+1];
	}
	tab->elements[tab->len - 1] = NULL;
	tab->len--;
}

//Deletes the specified table from memory.
void table_destroy(mtable* tab){
	free(tab->elements);
	free(tab);
}

//Inserts an element at the specified index of the given table, pushing
//all elements after one index.
void table_insert(mtable* tab, size_t index, void* el){
	table_append(tab, tab->elements[index]);
	for (int i = tab->len - 1; i > index; i--){
		tab->elements[i] = tab->elements[i-1];
	}
	tab->elements[index] = el;
}
