#ifndef M_STRING_H
#define M_STRING_H

typedef struct{
    char* str;
    int len;
} mstring;

mstring* string_new(const char* s);
mstring* string_concatenate(mstring* s1, mstring* s2);
mstring* string_sub(mstring* string, int i1, int i2);
mstring* string_tolower(const mstring* string);
mstring* string_reverse(const mstring* string);
void string_delete(mstring* string);

#endif /* M_STRING_H */
