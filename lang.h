#ifndef M_LANG_H
#define M_LANG_H

#include "mlist.h"
#include "mstring.h"

//TYPENAMES:
#define MTYPE_BOOLEAN   "boolean"
#define MTYPE_FLOAT     "real"
#define MTYPE_STRING    "string"
#define MTYPE_INT       "integer"
#define MTYPE_LIST      "list"
#define MTYPE_FUNCTION  "function"
#define MTYPE_CHAR      "character"
#define MTYPE_VOID      "nothing"

//RESERVED VERBOSE:
#define NEWVAR1   "new "
#define NEWVAR2   ": "
#define SETVAR1   "set "
#define SETVAR2   "to "
#define SETFUNC   "function "
#define BOOLTRUE  "true"
#define BOOLFALSE "false"

#define MAXIMUM_VAR_NAME_LENGTH  25
#define MAXIMUM_FUNC_NAME_LENGTH 25

typedef enum {
    MSTRING,
    MINTEGER,
    MREAL,
    MBOOLEAN,
    MCHARACTER,
    MLIST,
    MFUNCTION,
    MVOID
} mtype;

typedef enum {
    MFALSE,
    MTRUE
} mboolean;

typedef union { 
    long long integer; 
    mstring* string; 
    double real; 
    mlist* list;
    mboolean boolean;
    char character;
} mvalue;

typedef struct {
    mtype type;
    char* name;
    mvalue value;
} mvar;

typedef struct mstate{
    mlist* vars;
    mlist* funcs;
    struct mstate* parent;
} mstate;

typedef void (*mproc)(mstate*);

typedef struct {
	int argc;
	int procc;
	char* name;
	mtype rtype;
	mtype* argt;
    char** argname;
	mproc* procs;
} mfunc;

extern mvalue funcretr;
extern mstate* globalstate;
extern mstate* currentstate;

void  var_create(mstate* state, char* name, mtype type, mvalue value);
mstate* state_create(mstate* parent);
void state_destroy(mstate* state);
mtype str_to_type(char* type);
mvar* var_find(mstate* state, char* varname);
void  readcmd(char* cmd);
void func_create(mstate* state,
                 char*  name,
				 mtype  rtype,
				 mtype* argt,
				 int    argc,
				 mproc* proc,
				 int    procc);
void func_exec(mstate* state, mfunc* func);

#endif /* M_LANG_H */
