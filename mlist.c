#include "mlist.h"
#include <stdlib.h>
#include <stdio.h>

#define PTR void*

mlist* all_lists;

static list_slot_t* _GETSLOT(mlist* List, unsigned int index){
	int i;
	list_slot_t* el;
	if (index < LIST_INITIAL_INDEX){
		return NULL;
	}
	el = List->start;
	list_iterate(List) {
		if (el->index == index){
			return el;
		}
		el = el->next;
	}
	return NULL;
}

static void int_quicksort(int** array, int size){
	if (size <= 1) return;
	int pivot = *(array[0]);
	int	swap;
	int leftmark  = 1,
		rightmark = size-1;
	if (size == 2) {
		if (*(array[0]) > *(array[1])){
			swap = *(array[0]);
			*(array[0]) = *(array[1]);
			*(array[1]) = swap;
			return;
		}
		return;
	}
	while (leftmark < rightmark) {
		while (*(array[leftmark]) < pivot){
			leftmark++;
		}
		while (*(array[rightmark]) > pivot){
			rightmark--;
		}
		if (leftmark < rightmark){
			swap = *(array[leftmark]);
			*(array[leftmark]) = *(array[rightmark]);
			*(array[rightmark]) = swap;
		}
	}
	swap = pivot;
	*(array[0]) = *(array[rightmark]);
	*(array[rightmark]) = swap;
	int_quicksort(array, rightmark+1);
	int_quicksort(&(array[leftmark]), size - rightmark-1);
}

mlist* list_create(){
	mlist* ret = (mlist*)malloc(sizeof(mlist));
	if (ret == NULL) goto error; 
	ret->size = 0;
	ret->start = NULL;
	ret->last = NULL;
	if (all_lists == NULL) {
		all_lists = (mlist*)malloc(sizeof(mlist));
		if (all_lists == NULL) exit(1);
		all_lists->size = 0;
		all_lists->start = NULL;
		all_lists->last = NULL;
	}
	list_add(all_lists, ret);
	return ret;
error:
	printf("Error while trying to create mlist, out of memory.\n");
	return NULL;
}

PTR list_get(mlist* List, unsigned int index){
	int i;
	list_slot_t* el;
	if (index < LIST_INITIAL_INDEX){
		return NULL;
	}
	if (List->size - index - LIST_INITIAL_INDEX < List->size / 2){
		el = List->start;
		list_iterate(List) {
			if (el->index == index){
				return el->element;
			}
			//else...
			el = el->next;
		}
	} 
	else {
		el = List->last;
		list_iterate(List) {
			if (el->index == index){
				return el->element;
			}
			//else...
			el = el->previous;
		}
	}
	return NULL;
}

int list_get_index(mlist* List, PTR element){
	int i = LIST_INITIAL_INDEX;
	list_slot_t* _temp = List->start;
	list_iterate(List){
		if (_temp->element == element) return i;
		_temp = _temp->next;
		i++;
	}
	return -1;
}

void list_add(mlist* List, PTR element){
	if (List->size == 0){
		List->size++;
		List->start = (list_slot_t*)malloc(sizeof(list_slot_t));
		if (List->start == NULL) goto error;
		List->start->element = element;
		List->start->index = LIST_INITIAL_INDEX;
		List->start->previous = NULL;
		List->start->next = NULL;
		List->last = List->start;
	}
	else {
		list_slot_t* _temp;
		List->size++;
		_temp = List->last;
		List->last->next = (list_slot_t*)malloc(sizeof(list_slot_t));
		if (List->last->next == NULL) goto error;
		List->last = List->last->next;
		List->last->element = element;
		List->last->previous = _temp;
		List->last->index = List->size - 1 + LIST_INITIAL_INDEX;
	}
	return;
error:
	printf("Error when trying to add element '%p' to mlist '%p', out of memory.\n", element, List);
	List->size--;
	return;
}

void list_remove(mlist* List, unsigned int index){
	int i;
	list_slot_t* _slot = _GETSLOT(List, index);
	list_slot_t* _temp = _slot;

	for (i = _slot->index; i < List->size + LIST_INITIAL_INDEX; i++){
		_temp->index--;
		_temp = _temp->next;
	}

	if (List->start != _slot && List->last != _slot){
	_slot->previous->next = _slot->next;
	_slot->next->previous = _slot->previous;
	}

	if (List->last == _slot){
		List->last = _slot->previous;
	}
	else if (List->start == _slot){
		List->last = _slot->next;
	}
	free(_slot);
	List->size--;
}

PTR list_pop(mlist* List, unsigned int index){
	PTR ret = list_get(List, index);
	list_remove(List, index);
	return ret;
}

void list_reverse(mlist* List){
	list_slot_t* _temp_head = List->start;

	list_slot_t* iteratorEl = List->last;
	int i = LIST_INITIAL_INDEX;
	list_iterate(List){
		list_slot_t* _temp = iteratorEl->next;
		iteratorEl->index = i;
		iteratorEl->next = iteratorEl->previous;
		iteratorEl->previous = _temp;
		iteratorEl = iteratorEl->next;
		i++;
	}

	List->start = List->last;
	List->last = _temp_head;	
}

void list_replace_element(mlist* List, unsigned int index1, unsigned int index2){
	PTR _temp = list_get(List, index1);
	_GETSLOT(List, index1)->element = list_get(List, index2);
	_GETSLOT(List, index2)->element = _temp;
}

mlist* list_copy(mlist* List){
	mlist* ret = list_create();
	int i = 0;
	list_iterate(List){
		list_add(ret,list_get(List,i));
	}
	return ret;
}

void list_destroy(mlist* List){
	int i = 0;
	list_iterate(List){
		free(_GETSLOT(List,i));
		i++;
	}
	list_remove(all_lists, list_get_index(all_lists, List));
	free(List);
}

PTR* list_copy_to_array(mlist* List){
	PTR* ret = (PTR*)malloc(sizeof(PTR)*List->size);
	list_slot_t* el = List->start;
	int i = 0;
	if (ret == NULL){
		printf("Memory error");
		return NULL;
	}
	list_iterate(List){
		ret[i] = el->element;
		el = el->next;
		i++;
	}
	return ret;
}

void list_sort_int(mlist* List) {
	int** array = (int**)list_copy_to_array(List);
	int i = 0;
	list_slot_t* el = List->start;
	if (List->size <= 1){
		return;
	}
	int_quicksort(array, List->size);
	list_iterate(List){
		el->element = array[i];
		el = el->next;
		i++;		
	}
	free(array);
}
